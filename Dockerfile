FROM tigefa/bionic
MAINTAINER Sugeng Tigefa <sugeng.tigefa@gmail.com>

LABEL org.label-schema.vcs-url="https://gitlab.com/tigefa/bionic-brew"

RUN apt-get update -yqq && apt-get dist-upgrade -yqq
RUN apt-get install -yqq sudo bash bash-completion build-essential curl file git

RUN sudo adduser --disabled-password --gecos 'LinuxBrew' linuxbrew
# RUN useradd -ms /bin/bash linuxbrew
RUN echo 'linuxbrew ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN echo '%linuxbrew ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
USER linuxbrew
RUN sudo -u linuxbrew -H git clone https://github.com/Linuxbrew/brew.git $HOME/.linuxbrew
RUN sudo -u linuxbrew -H echo 'PATH="$HOME/.linuxbrew/bin:$PATH"' >> $HOME/.bashrc
RUN sudo -u linuxbrew -H echo 'PATH="$HOME/.linuxbrew/sbin:$PATH"' >> $HOME/.bashrc
RUN sudo -u linuxbrew -H echo 'export MANPATH="$(brew --prefix)/share/man:$MANPATH"' >> $HOME/.bashrc
RUN sudo -u linuxbrew -H echo 'export INFOPATH="$(brew --prefix)/share/info:$INFOPATH"' >> $HOME/.bashrc
# RUN sudo -u linuxbrew -H source ~/.bashrc
# RUN sudo -u linuxbrew -H brew update && brew doctor
# RUN sudo -u linuxbrew -H bash -c 'source $HOME/.bashrc && brew update && brew update && brew doctor'

# WORKDIR /home/linuxbrew/.linuxbrew
WORKDIR /home/linuxbrew/

ENTRYPOINT /bin/bash

# Clean up APT when done.
RUN sudo apt-get clean && sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
